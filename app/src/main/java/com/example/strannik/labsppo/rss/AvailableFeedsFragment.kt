package com.example.strannik.labsppo.rss


import android.content.ClipData
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import com.example.strannik.labsppo.R
import kotlinx.android.synthetic.main.content_available_feeds.*


class AvailableFeedsFragment : Fragment() {
    private fun copyFromTextViewToClipboard(textView: TextView) {
        val context = context ?: return

        val clipboardManager = getSystemService(context, android.content.ClipboardManager::class.java)

        val text = textView.text.toString()
        val clipData = ClipData.newPlainText("text", text)

        clipboardManager?.primaryClip = clipData

        Toast.makeText(context, "Copied!", Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_available_feeds, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        copy_first_feed.setOnClickListener {
            copyFromTextViewToClipboard(first_feed)
        }

        copy_second_feed.setOnClickListener {
            copyFromTextViewToClipboard(second_feed)
        }

        copy_third_feed.setOnClickListener {
            copyFromTextViewToClipboard(third_feed)
        }

        copy_fourth_feed.setOnClickListener {
            copyFromTextViewToClipboard(fourth_feed)
        }
    }
}
