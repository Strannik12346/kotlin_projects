package com.example.strannik.labsppo.profile

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.strannik.labsppo.MainActivity
import com.example.strannik.labsppo.R
import com.example.strannik.labsppo.authorization.AuthorizedUser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_edit_profile.*


class EditProfileFragment : Fragment() {
    private val GALLERY = 1
    private val CAMERA = 2

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivProfileEdit.setOnClickListener {
            showPictureDialog()
        }

        loadProfileOrNavigateUp()
    }

    private fun loadProfileOrNavigateUp() {
        val profile = AuthorizedUser.Profile

        if (profile == null) {
            val navController = Navigation.findNavController(activity!!, R.id.nav_host_fragment)
            NavigationUI.navigateUp(navController, drawer_layout)
            return
        }

        // Smart cast :)
        tvNameEdit.setText(profile.name)
        tvSurnameEdit.setText(profile.surname)
        tvMailEdit.setText(profile.email)
        tvPhoneEdit.setText(profile.phone)
        tvSkypeEdit.setText(profile.skype)
        ivProfileEdit.setImageBitmap(profile.photo)
    }

    private fun showPictureDialog() {
        val context = context ?: return

        val pictureDialog = AlertDialog.Builder(context)
        pictureDialog.setTitle("Select Action")

        val pictureDialogItems = arrayOf(
                "Select photo from gallery",
                "Capture photo from camera"
        )

        pictureDialog.setItems(pictureDialogItems
        ) { _, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }

        pictureDialog.show()
    }

    private fun saveProfile(newProfile: Profile, activity: MainActivity) {
        AuthorizedUser.Profile = newProfile
        ProfileContract.ProfileDbHelper(activity).UpdateProfile(newProfile)

        activity.listener?.onFragmentInteraction()

        Toast.makeText(activity, "Successfully saved!", Toast.LENGTH_LONG).show()
    }

    private fun revert(context: Context) {
        Toast.makeText(context, "Saved nothing . . . ", Toast.LENGTH_LONG).show()
    }

    private fun showSaveProfileDialog(newProfile: Profile) {
        val activity = activity as MainActivity

        AlertDialog.Builder(activity)
                .setMessage("Do you want to save changes?")
                .setPositiveButton("Yes")  {
                    _, _ -> saveProfile(newProfile, activity)

                }
                .setNegativeButton("No") {
                    _, _ -> revert(activity)
                }.show()
    }

    private fun choosePhotoFromGallery() {
        val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val act = activity ?: return

        // Checking for permissions first
        if (ContextCompat.checkSelfPermission(act, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(act, arrayOf(Manifest.permission.CAMERA), CAMERA)
        }
        else
        {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        val profile = AuthorizedUser.Profile ?: return

        val newProfile = Profile(
                _login = profile.login,
                _password = profile.password,
                _name = tvNameEdit.text.toString(),
                _surname = tvSurnameEdit.text.toString(),
                _email = tvMailEdit.text.toString(),
                _phone = tvPhoneEdit.text.toString(),
                _skype = tvSkypeEdit.text.toString(),
                _photo = (ivProfileEdit.drawable as BitmapDrawable).bitmap
        )

        showSaveProfileDialog(newProfile)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){
            CAMERA -> {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, CAMERA)
            }
        }
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val context = context ?: return

        when(requestCode) {
            GALLERY -> {
                if (data != null)
                {
                    val contentURI = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, contentURI)

                    if (bitmap != null) {
                        ivProfileEdit.setImageBitmap(bitmap)
                    }
                }
            }

            CAMERA -> {
                val bitmap = data?.extras?.get("data") as Bitmap?

                if (bitmap != null) {
                    ivProfileEdit.setImageBitmap(bitmap)
                }
            }
        }
    }
}
