package com.example.strannik.labsppo.rss


enum class Tag {
    Channel,
    Item,
    Title,
    Description,
    Link,
    PubDate,
    Enclosure,
    Url
}