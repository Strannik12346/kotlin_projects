package com.example.strannik.labsppo.authorization


import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import android.graphics.BitmapFactory
import android.widget.Toast
import com.example.strannik.labsppo.profile.Profile
import com.example.strannik.labsppo.profile.ProfileContract
import com.example.strannik.labsppo.R
import com.example.strannik.labsppo.rss.RssFeedCache


class LoginActivity : AppCompatActivity() {
    fun DefaultProfile(_login: String, _password: String) : Profile {
        val icon = BitmapFactory.decodeResource(resources, R.drawable.man)

        return Profile(
                _login,
                _password,
                "Name",
                "Surname",
                "mail@gmail.com",
                "+375330000000",
                "yourSkype123",
                icon
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        goToAuth()
    }

    private fun goToAuth() {
        password2.visibility = View.GONE
        action_button.text = "Sign in"
        change_action.text = "To registration"

        action_button.setOnClickListener {
            val profile = ProfileContract.ProfileDbHelper(this).LoadProfile(
                login.text.toString(),
                password1.text.toString()
            )

            if (profile != null) {
                AuthorizedUser.Profile = profile

                // Clearing the data
                RssFeedCache.LastFeed = null
                RssFeedCache.FeedUrl = null

                finish()
            }
            else {
                Toast.makeText(this, "Try to log in again!", Toast.LENGTH_LONG).show()
            }
        }

        change_action.setOnClickListener {
            goToRegister()
        }
    }

    private fun goToRegister() {
        password2.visibility = View.VISIBLE
        action_button.text = "Register"
        change_action.text = "To authorization"

        action_button.setOnClickListener {
            val helper = ProfileContract.ProfileDbHelper(this)

            val existing =  ProfileContract.ProfileDbHelper(this).LoadProfile(
                    login.text.toString(),
                    password1.text.toString()
            )

            // Passwords must match
            if (password1.text.toString() != password2.text.toString() ||
                    password1.text.length < 4 ||
                    existing != null) {

                Toast.makeText(this, "Passwords does not match!", Toast.LENGTH_LONG).show()

                password1.setText("")
                password2.setText("")
            }
            else {
                val profile = DefaultProfile(login.text.toString(), password1.text.toString())
                helper.SaveProfile(profile)

                Toast.makeText(this, "Successfully registered!", Toast.LENGTH_LONG).show()

                goToAuth()
            }
        }

        change_action.setOnClickListener {
            goToAuth()
        }
    }
}