package com.example.strannik.labsppo.about


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.strannik.labsppo.R
import com.example.strannik.labsppo.authorization.AuthorizedUser
import com.example.strannik.labsppo.authorization.LoginActivity
import com.example.strannik.labsppo.rss.RssFeedCache
import kotlinx.android.synthetic.main.content_home.*


class HomeFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    private fun setSignInOnClickListener() {
        signInOutButton.text = "Sign In"

        signInOutButton.setOnClickListener {
            // "Sign In" was pressed
            val intent = Intent(activity, LoginActivity::class.java)
            startActivityForResult(intent, 0)
        }
    }

    private fun setSignOutOnClickListener() {
        signInOutButton.text = "Sign Out"

        signInOutButton.setOnClickListener {
            // "Sign Out" was pressed
            AuthorizedUser.Profile = null
            setSignInOnClickListener()
            Toast.makeText(context, "Successfully logged out!", Toast.LENGTH_LONG).show()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (AuthorizedUser.Profile == null) {
            setSignInOnClickListener()
        }
        else {
            setSignOutOnClickListener()
        }

        dropDataButton.setOnClickListener {
            RssFeedCache.LastFeed = null
            RssFeedCache.FeedUrl = null
            Toast.makeText(activity, "Cache dropped!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (AuthorizedUser.Profile != null) {
            setSignOutOnClickListener()
            Toast.makeText(context, "Successfully authorized!", Toast.LENGTH_LONG).show()
        }
    }
}
