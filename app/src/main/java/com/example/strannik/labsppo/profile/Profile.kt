package com.example.strannik.labsppo.profile

import android.graphics.Bitmap


class Profile (_login : String, _password : String, _name : String, _surname : String,
               _email : String, _phone : String, _skype : String, _photo : Bitmap) {
    val login : String
    val password : String

    val name : String
    val surname : String

    val email : String
    val phone : String
    val skype : String

    val photo : Bitmap

    init{
        login = _login
        password = _password
        name = _name
        surname = _surname
        email = _email
        phone = _phone
        skype = _skype
        photo = _photo
    }
}