package com.example.strannik.labsppo.profile


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.strannik.labsppo.MainActivity
import com.example.strannik.labsppo.R
import com.example.strannik.labsppo.authorization.AuthorizedUser
import com.example.strannik.labsppo.authorization.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_profile.*


class ProfileFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if (AuthorizedUser.Profile == null) {
            val intent = Intent(activity, LoginActivity::class.java)
            startActivityForResult(intent, 0)
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnEdit.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.next_action, null)
        )

        (activity as MainActivity).listener = object : OnFragmentInteractionListener {
            override fun onFragmentInteraction() {
                tryLoadProfile()
            }
        }

        tryLoadProfile()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (AuthorizedUser.Profile == null) {
            val navController = Navigation.findNavController(activity!!, R.id.nav_host_fragment)
            NavigationUI.navigateUp(navController, drawer_layout)
        }
        else {
            tryLoadProfile()
        }
    }

    fun tryLoadProfile() {
        val profile = AuthorizedUser.Profile ?: return

        tvName.text = "${profile.name} ${profile.surname}"
        tvMail.text = profile.email
        tvPhone.text = profile.phone
        tvSkype.text = profile.skype
        ivProfile.setImageBitmap(profile.photo)
    }
}