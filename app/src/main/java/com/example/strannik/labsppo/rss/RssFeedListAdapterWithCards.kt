package com.example.strannik.labsppo.rss


import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.strannik.labsppo.R


class RssFeedListAdapterWithCards(private val mRssFeedModels: List<RssFeedModel>) : RecyclerView.Adapter<RssFeedListAdapterWithCards.FeedModelViewHolder>() {

    class FeedModelViewHolder(val rssFeedView: View) : RecyclerView.ViewHolder(rssFeedView)

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): FeedModelViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_rss_feed_card, parent, false)
        return FeedModelViewHolder(v)
    }

    override fun onBindViewHolder(holder: FeedModelViewHolder, position: Int) {
        val rssFeedModel = mRssFeedModels[position]

        if (rssFeedModel.enclosureUrl != null) {
            val imageView = holder.rssFeedView.findViewById(R.id.mediaTitle) as ImageView

            try {
                Glide.with(holder.rssFeedView).load(rssFeedModel.enclosureUrl).into(imageView)
            }
            catch(ex: Exception) {
                // ignored
            }
        }

        val title = holder.rssFeedView.findViewById<TextView>(R.id.titleText)
        title.text = rssFeedModel.title

        val description = holder.rssFeedView.findViewById<TextView>(R.id.descriptionText)
        description.text = rssFeedModel.description

        val link = holder.rssFeedView.findViewById<TextView>(R.id.linkText)
        link.text = rssFeedModel.link

        val pubDate = holder.rssFeedView.findViewById<TextView>(R.id.pubDate)
        pubDate.text = rssFeedModel.pubDate
    }

    override fun getItemCount(): Int {
        return mRssFeedModels.size
    }
}