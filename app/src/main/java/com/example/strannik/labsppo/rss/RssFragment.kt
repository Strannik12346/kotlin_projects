package com.example.strannik.labsppo.rss


import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.strannik.labsppo.R
import kotlinx.android.synthetic.main.content_rss.*
import android.content.Context.WINDOW_SERVICE
import android.content.Intent
import android.net.ConnectivityManager
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import android.app.Activity
import android.view.inputmethod.InputMethodManager


class RssFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rss, container, false)
    }

    private fun hideMenuAndFetchFeedLayout() {
        if (RssFeedCache.LastFeed != null) {
            fetchFeedLayout.visibility = View.GONE

            val menu = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
            menu?.visibility = View.GONE

            val inputManager = context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view?.windowToken, 0)
        }
    }

    private fun inflateRssFeed(mFeedModelList: List<RssFeedModel>) {
        val activ = activity ?: return

        val mRecyclerView = activ.findViewById<RecyclerView>(R.id.recyclerView)

        val orientation = activ.resources.configuration.orientation

        mRecyclerView.adapter =
                if (orientation == Configuration.ORIENTATION_PORTRAIT)
                    RssFeedListAdapter(mFeedModelList)
                else
                    RssFeedListAdapterWithCards(mFeedModelList)

        mRecyclerView.addOnItemTouchListener(object : RecyclerClickListener(activ) {
            override fun onItemClick(recyclerView: RecyclerView, itemView: View,
                                     position: Int) {

                val layoutManager = mRecyclerView.layoutManager as LinearLayoutManager
                val clicked = layoutManager.findViewByPosition(position)
                val link = clicked!!.findViewById<TextView>(R.id.linkText).text

                val intent = Intent(activity, BrowserActivity::class.java)
                intent.putExtra("link", link)
                activity?.startActivity(intent)
            }
        })

        hideMenuAndFetchFeedLayout()
    }

    private fun onPreExecuteListener() {
        swipeRefreshLayout?.isRefreshing = true
    }

    private fun onPostExecuteListener(success: Boolean) {
        swipeRefreshLayout.isRefreshing = false

        val activ = activity ?: return

        val cm = activ.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo

        if (activeNetwork == null || !activeNetwork.isConnected) {
            Toast.makeText(activ, "Internet connection not found.", Toast.LENGTH_SHORT).show()
        }

        if (success) {
            Toast.makeText(activ, "Loading RSS feed . . .", Toast.LENGTH_SHORT).show()

            // Inflating new info that was given by the task
            val lastFeed = RssFeedCache.LastFeed ?: return

            inflateRssFeed(lastFeed)
        }
        else {
            Toast.makeText(activ, "Valid RSS/Xml not found.", Toast.LENGTH_SHORT).show()

            // Checking our cache
            val lastFeed = RssFeedCache.LastFeed ?: return

            Toast.makeText(activ, "Showing last feed . . . ", Toast.LENGTH_SHORT).show()
            inflateRssFeed(lastFeed)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val orientation = activity?.resources?.configuration?.orientation

        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.layoutManager = LinearLayoutManager(activity)
        }
        else {
            val windowService = activity?.getSystemService(WINDOW_SERVICE) as WindowManager

            val display = windowService.defaultDisplay
            val width = display.width

            recyclerView.layoutManager = GridLayoutManager(activity, width / 500)
        }

        fetchFeedButton.setOnClickListener {
            FetchFeedTask(
                    rssFeedEditText.text.toString(),
                    this::onPreExecuteListener,
                    this::onPostExecuteListener
            ).execute(null)
        }

        swipeRefreshLayout.setOnRefreshListener {
            // Let's honour the site, which has the
            // RSS tutorial and a valid RSS feed . . .
            val lastFeedUrl = RssFeedCache.FeedUrl ?: "feed.androidauthority.com"

            FetchFeedTask(
                    lastFeedUrl,
                    this::onPreExecuteListener,
                    this::onPostExecuteListener
            ).execute(null)
        }

        val lastFeed = RssFeedCache.LastFeed ?: return
        inflateRssFeed(lastFeed)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        val bottomNav = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)

        bottomNav?.visibility = View.VISIBLE
    }
}
