package com.example.strannik.labsppo


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.strannik.labsppo.profile.OnFragmentInteractionListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    var listener : OnFragmentInteractionListener? = null

    private fun setupBottomNavMenu(navController: NavController) {
        bottom_nav?.let {
            NavigationUI.setupWithNavController(it, navController)
        }
    }

    private fun setupActionBar(navController: NavController) {
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    private val prefix = "/pages/"

    private fun handleIntent(navController: NavController) {
        // Url handling
        val intentData = intent.data

        if (intentData != null) {
            when (intentData.path) {
                prefix + "home"    -> return
                prefix + "about"   -> navController.navigate(R.id.imei_action)
                prefix + "profile" -> navController.navigate(R.id.profile_action)
                prefix + "first"   -> navController.navigate(R.id.first_action)
                prefix + "second"  -> navController.navigate(R.id.second_action)
                else -> {
                    val toast = Toast.makeText(this, intentData.path, Toast.LENGTH_LONG)
                    toast.show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Application started
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        // Adding navigation
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        setupBottomNavMenu(navController)
        setupActionBar(navController)

        handleIntent(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        val navigated = NavigationUI.onNavDestinationSelected(item!!, navController)
        return navigated || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.navigateUp(navController, drawer_layout)
        return super.onSupportNavigateUp()
    }
}