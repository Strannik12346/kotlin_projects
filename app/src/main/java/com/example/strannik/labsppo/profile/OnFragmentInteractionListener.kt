package com.example.strannik.labsppo.profile


interface OnFragmentInteractionListener {
    fun onFragmentInteraction()
}