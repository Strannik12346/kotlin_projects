package com.example.strannik.labsppo.authorization

import com.example.strannik.labsppo.profile.Profile


// lateinit does not help us when we
// try to check authorized user
object AuthorizedUser {
    var Profile : Profile? = null
}