package com.example.strannik.labsppo.about


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.strannik.labsppo.BuildConfig
import com.example.strannik.labsppo.R
import kotlinx.android.synthetic.main.content_imei.*


class ImeiFragment : Fragment() {
    private val MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0

    private fun makeToast(message: String) {
        val toast = Toast.makeText(activity!!, message, Toast.LENGTH_LONG)
        toast.show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_imei, container, false)
    }

    private fun showIMEI() {
        try {
            val telephonyManager = activity?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            var imei = telephonyManager.deviceId
            view?.findViewById<TextView>(R.id.LowerTextView)?.text = imei
        }
        catch(ex : SecurityException) {
            LowerTextView.text = "Just no IMEI today :(("
        }
    }

    private fun showVersion() {
        UpperTextView.text = BuildConfig.VERSION_NAME
    }

    private fun showExplanation() {
        // Initialize a new instance to show explanation
        val builder = AlertDialog.Builder(activity!!)

        // Set the alert dialog title
        builder.setTitle("Permissions")

        // Display a message on alert dialog
        builder.setMessage(
                "If you want to get IMEI, go to Settings -> Apps -> LabsPPO -> " +
                "Permissions and turn on the permissions 'Phone State'"
        )

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("YES") { dialog, which ->
            // Redirecting to settings
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", activity?.packageName, null)
            intent.data = uri
            startActivityForResult(intent, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE)
        }
    }

    override fun onStart() {
        super.onStart()

        // Checking for permissions first
        if (ContextCompat.checkSelfPermission(activity!!,
                        Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.READ_PHONE_STATE)) {
                showExplanation()
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.READ_PHONE_STATE),
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE)
            }
        }

        showIMEI()
        showVersion()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_PHONE_STATE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission was granted, yay!
                    makeToast("GRANTED")
                }
                else {
                    // Permission denied, boo!
                    makeToast("DENIED!")
                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_PHONE_STATE -> {
                showIMEI()
            }
        }
    }
}