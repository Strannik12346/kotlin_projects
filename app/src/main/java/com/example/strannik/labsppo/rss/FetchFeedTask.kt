package com.example.strannik.labsppo.rss

import android.text.TextUtils
import android.os.AsyncTask
import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import java.io.InputStream
import java.net.URL


class FetchFeedTask(_urlLink: String, _onPreExecuteFun: () -> Unit, _onPostExecuteFun: (success: Boolean) -> Unit) : AsyncTask<Void, Void, Boolean>() {
    private fun equalsIgnoreCase(first: String, second: String) : Boolean{
        return first.equals(second, ignoreCase = true)
    }

    private fun tryEnterChannel(name: String) : Tag? {
        if (equalsIgnoreCase(name, "Channel"))
            return Tag.Channel
        return null
    }

    private fun tryEnterItem(name: String) : Tag? {
        if (equalsIgnoreCase(name, "Item"))
            return Tag.Item
        return null
    }

    private fun tryEnterInnerItemTag(name: String) : Tag? {
        val list = listOf("Title", "Description", "Link", "PubDate", "Enclosure")
        for (tagName in list) {
            if (equalsIgnoreCase(name, tagName)) {
                return Tag.valueOf(tagName)
            }
        }
        return null
    }

    private fun tryEnterImageTag(name: String) : Tag? {
        val list = listOf("Url")
        for (tagName in list) {
            if (name == tagName) {
                return Tag.valueOf(tagName)
            }
        }
        return null
    }

    private fun tryGetImageUrlFromText(text: String) : String? {
        val patternStart = "<img src=\""
        val patternEnd = "\""

        val startIndex = text.indexOf(patternStart)
        val endIndex = text.indexOf(patternEnd, startIndex + patternStart.length + 1)

        return if (startIndex != -1)
            text.substring(startIndex + patternStart.length, endIndex)
            else null
    }

    private fun cleanText(text: String) : String {
        // Removing all the text
        var textToModify = text

        while (textToModify.contains('<')) {
            textToModify = textToModify.removeRange(
                    textToModify.indexOf('<'),
                    textToModify.indexOf('>') + 1
            )
        }

        return textToModify.filter { c -> c != '\t' || c != '\n' }
    }

    private fun tryEnterTag(outerTag: Tag?, innerTagName: String) : Tag? {
        if (outerTag == Tag.Enclosure && tryEnterImageTag(innerTagName) != null)
            return tryEnterImageTag(innerTagName)

        if (outerTag == Tag.Item && tryEnterInnerItemTag(innerTagName) != null)
            return tryEnterInnerItemTag(innerTagName)

        if (outerTag == Tag.Channel && tryEnterItem(innerTagName) != null)
            return tryEnterItem(innerTagName)

        if (outerTag == null && tryEnterChannel(innerTagName) != null)
            return tryEnterChannel(innerTagName)

        return null
    }

    private fun tryBackToItem(outerTag: Tag?, innerTagName: String) : Tag? {
        val list = listOf(Tag.Title, Tag.Description, Tag.Link, Tag.PubDate, Tag.Enclosure)
        for (tag in list) {
            if (outerTag == tag && equalsIgnoreCase(innerTagName, tag.toString())) {
                return Tag.Item
            }
        }
        return null
    }

    private fun tryBackFromTag(outerTag: Tag?, innerTagName: String) : Tag? {
        if (outerTag == Tag.Channel && equalsIgnoreCase(innerTagName, "Channel")) {
            return null
        }

        if (outerTag == Tag.Url && equalsIgnoreCase(innerTagName, "Url")) {
            return Tag.Enclosure
        }

        return tryBackToItem(outerTag, innerTagName)

    }

    private fun parseFeed(inputStream: InputStream): List<RssFeedModel> {
        var currentTag: Tag? = null
        val items = ArrayList<RssFeedModel>()

        var title: String? = null
        var link: String? = null
        var description: String? = null
        var pubDate: String? = null
        var enclosureUrl: String? = null

        try {
            val xmlPullParser = Xml.newPullParser()
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            xmlPullParser.setInput(inputStream, null)

            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                val eventType = xmlPullParser.eventType

                val name = xmlPullParser.name

                when(eventType) {
                    XmlPullParser.START_TAG -> {
                        val newTag = tryEnterTag(currentTag, name)
                        if (newTag != null)
                            currentTag = newTag
                    }

                    XmlPullParser.END_TAG -> {
                        if (equalsIgnoreCase(name, "Item")) {
                            val model = RssFeedModel(
                                    title!!,
                                    link!!,
                                    description!!,
                                    pubDate!!,
                                    enclosureUrl
                            )

                            items.add(model)

                            title = null
                            link = null
                            description = null
                            pubDate = null
                            enclosureUrl = null

                            currentTag = Tag.Channel
                        }
                        else {
                            val newTag = tryBackFromTag(currentTag, name)
                            if (newTag != null) {
                                currentTag = newTag
                            }
                        }
                    }

                    XmlPullParser.TEXT -> {
                        val result = xmlPullParser.text

                        // Trying to get image url, if text
                        val list = listOf(Tag.Item, Tag.Title, Tag.Link, Tag.Description, Tag.PubDate)
                        if (list.indexOf(currentTag) != -1) {
                            val url = tryGetImageUrlFromText(result)

                            if (url != null) {
                                enclosureUrl = url
                            }
                        }

                        when(currentTag) {
                            Tag.Title -> title = cleanText(result)
                            Tag.Link -> link = cleanText(result)
                            Tag.Description -> description = cleanText(result)
                            Tag.PubDate -> pubDate = cleanText(result)
                            Tag.Url -> enclosureUrl = cleanText(result)

                            else -> {
                                // Ignore all other tags
                            }
                        }
                    }
                }
            }

            return items
        }
        catch(ex: Exception) {
            throw ex
        }
        finally {
            inputStream.close()
        }
    }

    private var urlLink: String
    init {
        urlLink = _urlLink
    }

    private val onPreExecuteFun = _onPreExecuteFun

    private val onPostExecuteFun = _onPostExecuteFun

    override fun onPreExecute() {
        onPreExecuteFun()
    }

    override fun doInBackground(vararg voids: Void): Boolean? {
        if (TextUtils.isEmpty(urlLink))
            return false

        try {
            if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                urlLink = "http://$urlLink"

            val url = URL(urlLink)
            val inputStream = url.openConnection().getInputStream()

            RssFeedCache.LastFeed = parseFeed(inputStream)
            RssFeedCache.FeedUrl = urlLink
        }
        catch(ex : Exception) {
            return false
        }

        return true
    }

    override fun onPostExecute(success: Boolean?) {
        val succ = success ?: return

        onPostExecuteFun(succ)
    }
}