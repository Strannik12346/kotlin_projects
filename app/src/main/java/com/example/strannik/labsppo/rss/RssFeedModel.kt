package com.example.strannik.labsppo.rss

class RssFeedModel(
        var title: String,
        var link: String,
        var description: String,
        var pubDate: String,
        var enclosureUrl: String?
)