package com.example.strannik.labsppo.profile

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import com.example.strannik.labsppo.authorization.HashUtils


object ProfileContract {
    // Table contents are grouped together in an anonymous object.
    object ProfileEntry : BaseColumns {
        const val TABLE_NAME = "Profile"
        const val COLUMN_LOGIN = "Login"
        const val COLUMN_PASSWORD = "Password"
        const val COLUMN_NAME = "Name"
        const val COLUMN_SURNAME = "Surname"
        const val COLUMN_PHOTO = "Photo"
        const val COLUMN_EMAIL = "Email"
        const val COLUMN_PHONE = "Phone"
        const val COLUMN_SKYPE = "Skype"
    }

    private const val SQL_CREATE_QUERY =
            "CREATE TABLE ${ProfileEntry.TABLE_NAME}" +
                    "(" +
                    "${BaseColumns._ID}              INTEGER PRIMARY KEY," +
                    "${ProfileEntry.COLUMN_LOGIN}    TEXT," +
                    "${ProfileEntry.COLUMN_PASSWORD} TEXT," +
                    "${ProfileEntry.COLUMN_NAME}     TEXT," +
                    "${ProfileEntry.COLUMN_SURNAME}  TEXT," +
                    "${ProfileEntry.COLUMN_PHOTO}    BLOB," +
                    "${ProfileEntry.COLUMN_EMAIL}    TEXT,"  +
                    "${ProfileEntry.COLUMN_PHONE}    TEXT," +
                    "${ProfileEntry.COLUMN_SKYPE}    TEXT"  +
                    ");"


    private const val SQL_DELETE_QUERY = "DROP TABLE IF EXISTS ${ProfileEntry.TABLE_NAME};"

    class ProfileDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase) {
            db.execSQL(SQL_CREATE_QUERY)
        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            db.execSQL(SQL_DELETE_QUERY)
            onCreate(db)
        }

        override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            onUpgrade(db, oldVersion, newVersion)
        }

        companion object {
            const val DATABASE_VERSION = 1
            const val DATABASE_NAME = "SQLiteDatabase.db"
        }

        fun LoadProfile(login: String, password: String) : Profile? {
            val db = readableDatabase

            val projection = arrayOf(
                    ProfileEntry.COLUMN_LOGIN,
                    ProfileEntry.COLUMN_PASSWORD,
                    ProfileEntry.COLUMN_NAME,
                    ProfileEntry.COLUMN_SURNAME,
                    ProfileEntry.COLUMN_EMAIL,
                    ProfileEntry.COLUMN_PHONE,
                    ProfileEntry.COLUMN_SKYPE,
                    ProfileEntry.COLUMN_PHOTO
            )

            val selection = "${ProfileEntry.COLUMN_LOGIN}    = ? AND " +
                            "${ProfileEntry.COLUMN_PASSWORD} = ?     "
            val selectionArgs = arrayOf(login, HashUtils.sha512(password))

            val cursor = db.query(
                    ProfileEntry.TABLE_NAME,   // The table to query
                    projection,                // The array of columns to return (pass null to get all)
                    selection,                 // The columns for the WHERE clause
                    selectionArgs,             // The values for the WHERE clause
                    null,              // don't group the rows
                    null,               // don't filter by row groups
                    null               // don't order rows
            )

            if (cursor.count == 0) {
                return null
            }

            with(cursor) {
                moveToNext()

                return Profile(
                        _login = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_LOGIN)),
                        _password = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_PASSWORD)),
                        _name = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_NAME)),
                        _surname = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_SURNAME)),
                        _email = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_EMAIL)),
                        _phone = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_PHONE)),
                        _skype = getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_SKYPE)),
                        _photo = BitmapEncoderDecoder().stringToBitmap(
                                getString(getColumnIndexOrThrow(ProfileEntry.COLUMN_PHOTO))
                        )
                )
            }
        }

        fun SaveProfile(obj : Profile) {
            val db = writableDatabase

            db.execSQL(SQL_DELETE_QUERY)
            db.execSQL(SQL_CREATE_QUERY)

            // Create a new map of values, where column names are the keys
            val values = ContentValues().apply {
                put(ProfileEntry.COLUMN_LOGIN, obj.login)
                put(ProfileEntry.COLUMN_PASSWORD, HashUtils.sha512(obj.password))

                put(ProfileEntry.COLUMN_NAME, obj.name)
                put(ProfileEntry.COLUMN_SURNAME, obj.surname)
                put(ProfileEntry.COLUMN_EMAIL, obj.email)
                put(ProfileEntry.COLUMN_PHONE, obj.phone)
                put(ProfileEntry.COLUMN_SKYPE, obj.skype)

                put(ProfileEntry.COLUMN_PHOTO, BitmapEncoderDecoder().bitmapToString(obj.photo))
            }

            db.insert(ProfileEntry.TABLE_NAME, null, values)
        }

        fun UpdateProfile(obj : Profile) {
            val db = writableDatabase

            val values = ContentValues().apply {
                put(ProfileEntry.COLUMN_NAME, obj.name)
                put(ProfileEntry.COLUMN_SURNAME, obj.surname)
                put(ProfileEntry.COLUMN_EMAIL, obj.email)
                put(ProfileEntry.COLUMN_PHONE, obj.phone)
                put(ProfileEntry.COLUMN_SKYPE, obj.skype)
                put(ProfileEntry.COLUMN_PHOTO, BitmapEncoderDecoder().bitmapToString(obj.photo))
            }

            val selection = "${ProfileEntry.COLUMN_LOGIN} =    ? AND " +
                    "${ProfileEntry.COLUMN_PASSWORD} = ?     "
            val selectionArgs = arrayOf(obj.login, HashUtils.sha512(obj.password))

            db.update(
                    ProfileEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs
            )
        }
    }
}

